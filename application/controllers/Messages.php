<?php
namespace Controllers;

use Framework\Auth;
use Framework\Controller;
use Framework\Model;
use Framework\Request;

class Messages extends Controller
{

    public function index()
    {
        return $this->view->render('users/index.phtml');
    }


    public function add()
    {
        try {
            $postParams = Request::singletone()->getParams();

            $fileNameNew = null;

            if (!empty($_FILES['fileToUpload']['name'])) {
                $file = $_FILES['fileToUpload'];

                $fileName  = $file['name'];
                $fileTmp   = $file['tmp_name'];
                $fileSize  = $file['size'];
                $fileError = $file['error'];

                $fileExt = explode('.', $fileName);
                $fileExt = strtolower(end($fileExt));

                $allowed = array('png', 'jpg');

                if (in_array($fileExt, $allowed)) {
                    if ($fileError === 0) {
                        if ($fileSize <= 200000) {
                            $fileNameNew     = uniqid() . '.' . $fileExt;
                            $fileDestination = ROOT . 'public/uploads/' . $fileNameNew;
                            if (move_uploaded_file($fileTmp,  $fileDestination)) {
                            } else {
                                throw new \Exception("File wasn't upload");
                            }
                        } else {
                            throw new \Exception("File wasn't upload. File is too large");
                        }
                    } else {
                        throw new \Exception("File wasn't upload");
                    }
                } else {
                    throw new \Exception("File wasn't upload. " . $fileExt . " extension is not supported.");
                }

            }

            $user = new Auth();
            $id = $user->getIdentity()['userid'];

            $newMess = new \Models\Messages();
            $insertResult = $newMess->add($id, $postParams['comment'], $fileNameNew);
            $getLastMessage = $newMess->getById($insertResult);
        } catch (\Exception $e) {
            $getLastMessage = ['error' => $e->getMessage()];
        }
        echo json_encode($getLastMessage);
    }

    public function delete()
    {
        $del_id = Request::singletone()->getParams();
        $del_id = $del_id['delete_id'];
        $mes = new \Models\Messages();
        $mes->delete($del_id);
    }
}