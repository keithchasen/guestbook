<?php
namespace Controllers;

use Framework\Auth;
use Framework\Controller;
use Framework\Request;
use Models\Users;

class Register extends Controller
{

    public function index()
    {
        return $this->view->render('registration.phtml');
    }

    public function run()
    {
        $errorRegister =(['error' => null]);

        $postParams = Request::singletone()->getParams();

        foreach ($postParams as $key=>$value) {
            if($postParams[$key] == null or empty($postParams[$key])) {
                $errorRegister['error'] = "Fill in all the fields in the form";
                break;
            } elseif (!preg_match('/^[a-zA-Zа-яА-Я]+$/ui', $postParams['name'])) {
                $errorRegister['error'] = "Name should contain only letters!";

            } elseif (!preg_match('^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$^',$postParams['email'])) {
                $errorRegister['error'] = "Please enter a valid email!";
                break;
            } elseif (empty($postParams['pass']) || $postParams['pass'] != $postParams['r_pass']) {
                $errorRegister['error'] = "Confirm pass should be the same as Password!";
                break;
            }

        }

        if($errorRegister['error'] == null) {
            if ($postParams['pass'] != $postParams['r_pass']) {
                $errorRegister['error'] = "passwords don't match!";

            } else {
                $user = new Users();
                $id = $user->register($postParams);

                if ($id != null) {
                    $auth = new Auth();
                    $auth->setIdentity(['username' => $postParams['login'], 'useremail' => $postParams['email'], 'userid' => $id]);
                    $errorRegister['error'] = null;
                } else {
                    $errorRegister['error'] = 'Such user is already exists';
                }
            }
        }

        echo json_encode($errorRegister);
    }
}
