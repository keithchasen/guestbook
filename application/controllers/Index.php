<?php
namespace Controllers;

use Framework\Auth;
use Framework\Controller;
use Framework\Session;
use Models\Messages;
use Models\Users;

class Index extends Controller
{

    public function index()
    {
        $session = Session::getInstance();

        $auth = new Auth();
        $logged = $auth->getIdentity();
        if ($logged == false) {
            $session->unSession();
            header('location: ../login');
            exit;
        }

        $messages = new Messages();
        $this->view->data = $messages->getComments();
        return $this->view->render('users/index.phtml');
    }

    public function logout()
    {
        Session::getInstance()->unSession();
        $auth = new Auth();
        $auth->setIdentity(false);
        header('location: ../index');
    }


}