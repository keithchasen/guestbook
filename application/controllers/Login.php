<?php
namespace Controllers;

use Framework\Auth;
use Framework\Controller;
use Framework\Request;
use Framework\Session;
use Models\Messages;
use Models\Users;

class Login extends Controller
{

    public function index()
    {
        return $this->view->render('login.phtml');
    }

    public function run()
    {
        $status = null;
        $postParams = Request::singletone()->getParams();

        try {
            $model = new Users();
            $data = $model->login($postParams);

            if(!empty($data)) {
                $auth = new Auth();
                $auth->setIdentity(['username' => $postParams['login'], 'useremail' => $data[0]['email'], 'userid' =>$data[0]['id']]);
                $status = (['error' => false]);

            } else {
                $status = (['error' => true]);
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        echo json_encode($status);
    }
}