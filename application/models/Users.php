<?php
namespace Models;

use Framework\Auth;
use Framework\Model;
use Framework\Session;

class Users extends Model
{

    public function login($credsArray)
    {
        $query = "SELECT * FROM users WHERE name = :login AND pass = :pass";
        $creds = ['login' => $credsArray['login'], 'pass' => MD5($credsArray['pass'])];
        $data = $this->db->fetchAll($query, $creds);
        return $data;
    }

    public function register($credsArray)
    {
        $registerError = null;

        $query = "SELECT * FROM users WHERE name = :login AND email = :email";
        $creds = ['login' => $credsArray['login'], 'email' => $credsArray['email']];
        $data = $this->db->fetchAll($query, $creds);

        if ($data == null) {
            $query = "INSERT INTO users (name, email, pass) VALUES (:login, :email, :pass)";
            $creds = ['login' => $credsArray['login'], 'email' => $credsArray['email'], 'pass' => MD5($credsArray['pass'])];
            $id = $this->db->insert($query, $creds);

            return $id;
        }
        return null;
    }
}

