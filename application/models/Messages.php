<?php
namespace Models;

use Framework\Model;
use Framework\Request;

class Messages extends Model
{

    public function getComments()
    {
        $query = "SELECT u.id, u.name, u.email, m.comment_id AS com_id, m.comment, m.date, m.image
                                 FROM users u, message m
                                 WHERE m.user_id = u.id";
        $data = $this->db->fetchAll($query);
        return $data;
    }

    public function getById($data)
    {
        $params = ['id' => $data];
        $query = "SELECT * FROM message WHERE comment_id = :id";
        $data = $this->db->fetchAll($query, $params);
        return $data;
    }

    public function add($userid, $message, $imagepath)
    {
        $params = ['userid' => $userid, 'mess' => $message, 'imagepath' => $imagepath];
        $query = "INSERT INTO message (user_id, comment, image) VALUES (:userid, :mess, :imagepath)";
        $add = $this->db->insert($query, $params);
        return $add;
    }

    public function delete($data)
    {
        $query = "DELETE FROM message WHERE comment_id = :mesId";
        $params = ['mesId' => $data];
        $this->db->delete($query, $params);
    }
}


