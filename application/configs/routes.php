<?php
return array(
    '' => 'index/index',
    'index' => 'index/index',
    'login' => 'login/index',
    'login/run' => 'login/run',
    'logout' => 'index/logout',
    'register' => 'register/index',
    'register/run' => 'register/run',
    'messages/delete' => 'messages/delete',
    'messages/add' => 'messages/add',
    'user' => 'users/index'
);

