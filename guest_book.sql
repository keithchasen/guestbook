-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 24, 2016 at 09:30 PM
-- Server version: 5.7.11-0ubuntu6
-- PHP Version: 7.0.4-7ubuntu2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `guest_book`
--

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date` text NOT NULL,
  `comment_id` int(11) NOT NULL,
  `image` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`user_id`, `comment`, `date`, `comment_id`, `image`) VALUES
(126, '123', '2016.03.14 14:46', 120, ''),
(126, '123123123', '2016.03.14 14:46', 121, 'pug2.jpg'),
(126, '123123213', '2016.03.14 14:55', 124, 'pug.jpg'),
(116, '123', '2016.03.02 14:40', 110, '56d6df50227f3pug.jpg'),
(126, 'Hello', '2016.03.21 14:36', 191, '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pass` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `pass`) VALUES
(1, 'Alex', 'Alex@gmail.com', ''),
(117, 'Alex', 'asdads@mail.ru', ''),
(116, 'Alex1', 'alex1@gmail.com', ''),
(118, 'AlexT', 'alex@gmail.com', '202cb962ac59075b964b07152d234b70'),
(127, 'zzz', 'zzz@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(120, '', '', 'd41d8cd98f00b204e9800998ecf8427e'),
(121, '', '', 'd41d8cd98f00b204e9800998ecf8427e'),
(122, '', '', 'd41d8cd98f00b204e9800998ecf8427e'),
(123, '', '', 'd41d8cd98f00b204e9800998ecf8427e'),
(124, '', '', 'd41d8cd98f00b204e9800998ecf8427e'),
(125, 'asd', 'asdads@mail.ru', 'a8f5f167f44f4964e6c998dee827110c'),
(126, '111', '1asdads@mail.ru', 'a8f5f167f44f4964e6c998dee827110c'),
(128, 'asd', 'sadasd@ada.ru', 'a8f5f167f44f4964e6c998dee827110c'),
(129, '111', '1asdads@mail.ru', ''),
(130, '111', '1asdads@mail.ru', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
