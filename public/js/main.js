$(document).ready(function () {
//счетчик комментариев
    function count() {
        var n = $(".panel").length;
        $(".badge").text(n);
    }

    count();

    $('body')

        .on('click','.showMore', function () {

            $(this).parents('.panel-default').find('.commentText').toggleClass('showText');

            if ($(this).html() == "Show less...") {
                $(this).html("Show more...");

            } else {

                $(this).html("Show less...");
            }
        })

        .on("click", ".delete", function () {
            var path = $(this).parents('.panel-default');
            var id = path.find('.comment').attr('id');
            path.toggleClass("toDel");

            $(".del").click(function () {

                $.ajax({
                    type: 'POST',
                    url: 'messages/delete/',
                    data: {delete_id: id},
                    success: function () {
                        $(".toDel").remove();
                        count();
                    },
                    error: function() {
                        alert("Error! Cannot delete message");
                    }
                })
            })
        })

        .on("click", ".add", function (e) {
            e.preventDefault();
            var name = $('#user_name').html();
            var formData = new FormData($('#formsend')[0]);

            if (msg == 0) {
                alert('Enter some text!');

            } else {
                $.ajax({
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    url: '/messages/add/',
                    dataType: 'json',
                    data: formData,
                    success: function (data) {
                        if (data.error != null) {
                            alert('Error adding');
                        } else {

                            var maxLength = 100;

                            var commentTextClass = (data[0].comment.length > maxLength) ? 'hideText' : '';
                            var hide = (data[0].comment.length > maxLength) ? '' : 'hidden';


                           var html =  "<div class='panel panel-default'>" +
                                "<div class='panel-heading'>" +
                                "<b class='comment' id='"+data[0].comment_id+"'>#"+data[0].comment_id+" "+name+"</b>" +
                                "<b class='pull-right'>"+data[0].date+"</b></div>" +
                                "<div class='panel-body commentText "+commentTextClass+"'>";

                                    if(data[0].image != null) {
                                       html += "<img class='pull-left' width='128px' height='auto' src='uploads/"+ data[0].image +"'>";
                                    }

                                html += data[0].comment + "</div>" +
                                    "<div class='panel-footer'>" +
                                    "<button type='button' class='btn btn-primary showMore pull-left "+hide+"'>Show More...</button>" +
                                    "<button type='button' class='btn btn-primary delete' data-toggle='modal' data-target='#myModal'>" +
                                    "<i class='glyphicon glyphicon-remove'></i> Delete" +
                                    "</button>" +
                                    "</div>" +
                                    "</div>";

                            $('.sms').append(html);
                        count()
                        }
                    },
                    error: function () {
                        alert('An error occurred while adding a comment')
                    }
                });
            }
        })
});



