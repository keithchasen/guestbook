$(document).ready(function(){

    $("body").on("click", "#register", function (e) {
            e.preventDefault();

            var login  = $('[name=login]').val();
            var email  = $('[name=email]').val();
            var pass   = $('[name=pass]').val();
            var r_pass = $('[name=r_pass]').val();


            $.ajax({
                url: "register/run",
                type: "POST",
                data: {login: login, email: email, pass: pass, r_pass: r_pass},
                dataType: 'json',
                success: function(response) {

                    if(response.error == null) {
                        $(location).attr('href', "/index");
                        $('div.alert-danger').hide();
                    } else {
                        $('div.alert-danger').show().text(response.error);
                    }
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText + '|\n' + status + '|\n' +error);
                }
            });
        })

        .on('click', '#login', function (e) {
            e.preventDefault();

            var login = $('[name=login]').val();
            var pass = $('[name=pass]').val();
            var data =  {login: login, pass: pass};


            $.ajax({
                url: "login/run",
                type: "POST",
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('div.alert-info').show().text("Authentication. Please Wait.");
                },
                success: function(data) {
                    if(data.error == true) {
                        $('div.alert-danger').show().text("Use valid credentials to login");
                        $('div.alert-info').hide();

                    } else {
                        $(location).attr('href', "/index");
                        $('div.alert-danger').hide();
                    }
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText + '|\n' + status + '|\n' +error);
                }
            });
        });
});
