<?php

define('ROOT', dirname(__DIR__) . '/');
define('HOST', $_SERVER['SERVER_NAME'] . '/');

require '../vendor/Autoloader.php';

$app = new \Framework\Application();

try {
    echo $app->process();
} catch (Exception $e) {
    echo $e->getMessage();
}


